#include "../../include/container/linkqueue.h"

int init_link_queue(LinkQueue *queue, int datasize)
{
    if (queue == NULL)
    {
        return 0;
    }

    return init_link_list(&(queue->list), datasize);
}
int push_link_queue(LinkQueue *queue, void *data)
{
    if (queue == NULL)
    {
        return 0;
    }

    return add_link_tail(&(queue->list), data);
}

int pop_link_queue(LinkQueue *queue)
{
    if (queue == NULL)
    {
        return 0;
    }

    return delect_link_node(&(queue->list), queue->list.head);
}

void *get_link_front(LinkQueue *queue)
{
    if (queue == NULL)
    {
        return 0;
    }

    return queue->list.head->data;
}

int destroy_link_queue(LinkQueue *queue)
{
    if (queue == NULL)
    {
        return 0;
    }

    return destroy_link_list(&(queue->list));
}
