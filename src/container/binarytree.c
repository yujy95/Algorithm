#include "../../include/container/binarytree.h"

int init_bin_tree(BinTree *binTree, int datasize)
{
    if (binTree == NULL)
    {
        printf("init failed!tree is null.\n");
        return 0;
    }
    binTree->datasize = datasize;
    binTree->storey = 0;
    init_seq_list(&(binTree->storey_list), sizeof(LinkList)); //初始化层列表
    add_node_list(binTree);                                   //增加第一层链表

    return 1;
}

int add_bin_node(BinTree *binTree, int storey, int position, void *data)
{
    if (binTree == NULL)
    {
        printf("add failed!tree is null.\n");
        return 0;
    }
    if (storey > binTree->storey + 1)
    {
        printf("add failed!storey error.\n");
        return 0;
    }
    if (position > pow(2, storey - 1) - 1)
    {
        printf("add failed!position error.\n");
        return 0;
    }

    Node *node = search_node_bin(binTree, storey, position);
    if (node != NULL) //节点位置存在
    {
        printf("add failed!node error.\n");
        return 0;
    }

    LinkList *node_list = binTree->storey_list.node; //层链表
    if (storey == 1)                                 //如果是创建根节点
    {
        BiTNode rootNode; //新建根节点
        rootNode.lchild = NULL;
        rootNode.rchild = NULL;
        rootNode.parent = NULL;
        rootNode.position = position;
        rootNode.data = malloc(binTree->datasize);
        memcpy(rootNode.data, data, binTree->datasize);
        add_link_head(node_list, &rootNode);
        return 1;
    }

    int parent_storey = storey - 1;     //父节点层数
    int parent_position = position / 2; //父节点位置
    node_list += parent_storey - 1;     //父节点所在层链表
    node = search_node_bin(binTree, parent_storey, parent_position);
    if (node == NULL) //父节点不存在
    {
        printf("add failed!parent node error.\n");
        return 0;
    }
    BiTNode *parent_node = node->data;

    if (storey == binTree->storey + 1) //如果是下一层
    {
        add_node_list(binTree); //增加层链表
    }
    node_list = binTree->storey_list.node;
    node_list += storey - 1; //本节点所在层链表

    BiTNode newNode; //新建树节点
    newNode.lchild = NULL;
    newNode.rchild = NULL;
    newNode.parent = parent_node;
    newNode.position = position;
    newNode.data = malloc(binTree->datasize);
    memcpy(newNode.data, data, binTree->datasize);

    if (position == 0 || node_list->length == 0) //第一个直接头部插入
    {
        add_link_head(node_list, &newNode);
    }
    else //其他先根据序号找到对应位置
    {
        point_before(node_list, position);
        insert_after(node_list, node_list->p, &newNode);
    }

    if (position % 2 == 0) //左孩子
    {
        parent_node->lchild = node_list->p->data;
    }
    else //右孩子
    {
        parent_node->rchild = node_list->p->data;
    }

    return 1;
}

int delect_bin_node(BinTree *binTree, int storey, int position)
{
    if (binTree == NULL)
    {
        printf("delect failed!tree is null.\n");
        return 0;
    }
    if (storey > binTree->storey)
    {
        printf("delect failed!storey is null.\n");
        return 0;
    }

    Node *node = search_node_bin(binTree, storey, position); //本节点
    if (node == NULL)
    {
        printf("delect failed!node error.\n");
        return 0;
    }

    LinkList *node_list = binTree->storey_list.node;
    node_list += storey - 1; //本节点所在层链表

    BiTNode *biTNode = node->data; //本节点
    if (biTNode->lchild != NULL)   //递归删除左子树
    {
        delect_bin_node(binTree, storey + 1, biTNode->lchild->position);
    }
    if (biTNode->rchild != NULL) //递归删除右子树
    {
        delect_bin_node(binTree, storey + 1, biTNode->rchild->position);
    }

    free(biTNode->data);
    delect_link_node(node_list, node); //删除本节点
    if (node_list->length == 0)      //如果是本层最后一个节点，删除本层
    {
        delect_seq_node(&(binTree->storey_list), storey - 1);
        (binTree->storey)--;
    }

    return 1;
}

int delect_last_storey(BinTree *binTree)
{
    if (binTree == NULL)
    {
        printf("destroy failed!tree is null.\n");
        return 0;
    }
    LinkList *node_list = binTree->storey_list.node;
    node_list += binTree->storey - 1; //最后一层链表

    node_list->p = node_list->head; //从头节点开始
    BiTNode *biTNode = NULL;
    Node *next = NULL;
    while (node_list->p != NULL)
    {
        biTNode = node_list->p->data; //每个节点的树节点
        next = node_list->p->next;    //下一个节点
        free(biTNode->data);          //删除树节点数据
        free(node_list->p->data);     //删除树节点
        free(node_list->p);           //删除节点
        node_list->p = next;
    }
    (binTree->storey)--;
    return 1;
}

BiTNode *search_bin_node(BinTree *binTree, int storey, int position)
{
    if (binTree == NULL)
    {
        printf("search failed!tree is null.\n");
        return NULL;
    }
    if (storey > binTree->storey)
    {
        printf("search failed!storey error.\n");
        return NULL;
    }

    Node *node = search_node_bin(binTree, storey, position);
    if (node == NULL)
    {
        printf("search failed!node error.\n");
        return NULL;
    }
    return node->data;
}

int modify_bin_node(BinTree *binTree, int storey, int position, void *data)
{
    if (binTree == NULL)
    {
        printf("modify failed!tree is null.\n");
        return 0;
    }
    if (storey > binTree->storey)
    {
        printf("modify failed!storey error.\n");
        return 0;
    }

    Node *node = search_node_bin(binTree, storey, position);
    BiTNode *biTNode = node->data;
    if (node == NULL)
    {
        printf("modify failed!node error.\n");
        return 0;
    }
    memcpy(biTNode->data, data, binTree->datasize);

    return 1;
}

int destroy_bin_tree(BinTree *binTree)
{
    if (binTree == NULL)
    {
        printf("destroy failed!tree is null.\n");
        return 0;
    }

    while (binTree->storey != 0)
    {
        delect_last_storey(binTree); //删除最后一层
    }

    destroy_seq_list(&(binTree->storey_list)); //删除层列表
    return 1;
}

int add_node_list(BinTree *binTree)
{
    if (binTree == NULL)
    {
        return 0;
    }
    if (&(binTree->storey_list) == NULL)
    {
        return 0;
    }
    LinkList *node_list = malloc(sizeof(LinkList));
    init_link_list(node_list, sizeof(BiTNode));
    insert_seq_node(&(binTree->storey_list), binTree->storey_list.length, node_list);
    (binTree->storey)++;

    return 1;
}

Node *search_node_bin(BinTree *binTree, int storey, int position)
{
    if (binTree == NULL)
    {
        return NULL;
    }
    LinkList *node_list = binTree->storey_list.node;
    node_list += storey - 1;        //对应层链表
    node_list->p = node_list->head; //指向头节点
    BiTNode *node;                  //树节点
    while (node_list->p != NULL)
    {
        node = node_list->p->data;
        if (node->position == position)
        {
            return node_list->p;
        }
        node_list->p = node_list->p->next;
    }
    return NULL;
}

int point_before(LinkList *node_list, int position)
{
    node_list->p = node_list->head;
    BiTNode *node;
    BiTNode *node_next;
    if (node_list->p == NULL)
    {
        return 0;
    }
    while (node_list->p->next != NULL)
    {
        node = node_list->p->data;
        node_next = node_list->p->next->data;
        if (node->position < position && node_next->position > position)
        {
            return 1;
        }
        node_list->p = node_list->p->next;
    }

    node = node_list->p->data;
    return node->position < position;
}
