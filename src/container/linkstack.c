#include "../../include/container/linkstack.h"

int init_link_stack(LinkStack *stack, int datasize)
{
    if (stack == NULL)
    {
        return 0;
    }

    return init_link_list(&(stack->list), datasize);
}

int push_link_stack(LinkStack *stack, void *data)
{
    if (stack == NULL)
    {
        return 0;
    }
    return add_link_head(&(stack->list), data);
}

int pop_link_stack(LinkStack *stack)
{
    if (stack == NULL)
    {
        return 0;
    }
    return delect_link_node(&(stack->list), stack->list.head);
}

void *get_link_top(LinkStack *stack)
{
    if (stack == NULL)
    {
        return NULL;
    }
    if (stack->list.head == NULL)
    {
        return NULL;
    }
    return stack->list.head->data;
}

int destroy_link_stack(LinkStack *stack)
{
    if (stack == NULL)
    {
        return 0;
    }
    destroy_link_list(&(stack->list));
    return 1;
}
