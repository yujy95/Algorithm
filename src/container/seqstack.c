#include "../../include/container/seqstack.h"

int init_seq_stack(SeqStack *stack, int datasize)
{
    if (stack == NULL)
    {
        return 0;
    }

    return init_seq_list(&(stack->list), datasize);
}

int push_seq_stack(SeqStack *stack, void *data)
{
    if (stack == NULL)
    {
        return 0;
    }

    return insert_seq_node(&(stack->list), stack->list.length, data);
}

int pop_seq_stack(SeqStack *stack)
{
    if (stack == NULL)
    {
        return 0;
    }

    return delect_seq_node(&(stack->list), stack->list.length);
}

void *get_seq_top(SeqStack *stack)
{
    if (stack == NULL)
    {
        return 0;
    }

    return (stack->list.node) + (stack->list.rear) * (stack->list.nodesize);
}

int destroy_seq_stack(SeqStack *stack)
{
    if (stack == NULL)
    {
        return 0;
    }

    return destroy_seq_list(&(stack->list));
}
