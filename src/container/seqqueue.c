#include "../../include/container/seqqueue.h"

int init_seq_queue(SeqQueue *queue, int datasize)
{
    if (queue == NULL)
    {
        return 0;
    }

    return init_seq_list(&(queue->list), datasize);
}
int push_seq_queue(SeqQueue *queue, void *data)
{
    if (queue == NULL)
    {
        return 0;
    }
    return insert_seq_node(&(queue->list), queue->list.length, data);
}

int pop_seq_queue(SeqQueue *queue)
{
    if (queue == NULL)
    {
        return 0;
    }
    return delect_seq_node(&(queue->list), 0);
}

void *get_seq_front(SeqQueue *queue)
{
    if (queue == NULL)
    {
        return 0;
    }
    return (queue->list.node) + (queue->list.front) * (queue->list.nodesize);
}

int destroy_seq_queue(SeqQueue *queue)
{
    if (queue == NULL)
    {
        return 0;
    }

    return destroy_seq_list(&(queue->list));
}
