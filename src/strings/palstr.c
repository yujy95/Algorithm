#include "../../include/strings/palstr.h"

void pal_str()
{
    char s[NUM];
    int l;

    printf("Please input a string(<=50):\n");
    scanf("%s", s);

    l = get_len(s);

    for (int i = 0; i < l / 2; i++)
    {
        if (*(s + i) != *(s + l - 1 - i))
        {
            printf("It's not palindrome string!\n");
            return;
        }
    }

    printf("It's palindrome string!\n");
}

int get_len(char *s)
{
    int l = 0;
    while (l < 50)
    {
        if (*(s + l) == '\0')
        {
            break;
        }
        l++;
    }

    return l;
}
