#include "../../include/strings/robotjump.h"

void road_input()
{
    int t = 0;       //行数
    char s[STR_LEN]; //轨道

    while (t <= 0)
    {
        printf("Please input row（0 < ）:");
        scanf("%d", &t);
    }

    printf("Please input road:\n");
    getchar();
    char c;
    int l = 0;
    for (int i = 0; i < STR_LEN; i++)
    {
        c = getchar();
        if (c != 'R' && c != 'L' && c != '\n')
        {
            printf("Input error.\n");
            return;
        }

        if (c == '\n')
        {
            *(s + i) = '\0';
            l++;
            if (l == t) //输入完毕
            {
                robot_jump(s, t);
                return;
            }
        }
        else
        {
            *(s + i) = c;
        }
    }
    printf("Out of limit.\n"); //超出限制
}

void robot_jump(char *s, int t)
{
    if (t < 1)
    {
        printf("Road error!\n");
        return;
    }
    int m = 0, n = 0;
    char c;
    int l = 0;
    for (int i = 0; i < STR_LEN; i++)
    {
        c = *(s + i);
        switch (c)
        {
        case 'L':
            n++;
            break;
        case 'R':
            m = n > m ? n : m;
            n = 0;
            break;
        case '\0':
            l++;
            m = n > m ? n : m;
            printf("%d\n", m + 1);
            m = n = 0;
            if (l == t)
            {
                return;
            }
            break;
        default:
            printf("Jump error!\n");
            return;
        }
    }
    printf("Jump error!\n");
}