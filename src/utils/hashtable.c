#include "../../include/utils/hashtable.h"

int init_hash_table(Hashtable *hashTable, ValueType valueType, int table_size)
{
    if (hashTable == NULL)
    {
        return 0;
    }

    hashTable->table_size = table_size;
    hashTable->value_list = malloc(table_size * sizeof(LinkList));

    switch (valueType)
    {
    case INT:
        hashTable->value_size = sizeof(int);
        hashTable->fun = hash_int;
        for (int i = 0; i < table_size; i++)
        {
            init_link_list((hashTable->value_list) + i, sizeof(int));
        }
        break;
    case STRING:
        hashTable->value_size = sizeof(char *);
        hashTable->fun = hash_string;
        for (int i = 0; i < table_size; i++)
        {
            init_link_list((hashTable->value_list) + i, sizeof(char *));
        }
        break;
    default:
        return 0;
    }

    return 1;
}

int inseart_hash_node(Hashtable *hashTable, void *value)
{
    if (hashTable == NULL)
    {
        return 0;
    }

    int key = (hashTable->fun)(hashTable, value);
    LinkList *list = hashTable->value_list + key;

    return add_link_tail(list, value);
}

int delect_hash_node(Hashtable *hashTable, void *value)
{
    if (hashTable == NULL)
    {
        return 0;
    }

    int key = (hashTable->fun)(hashTable, value);
    LinkList *list = hashTable->value_list + key;

    Node *node = search_node_h(hashTable, value);

    return delect_link_node(list, node);
}

void *search_hash_node(Hashtable *hashTable, void *value)
{
    if (hashTable == NULL)
    {
        return NULL;
    }

    Node *node = search_node_h(hashTable, value);
    if (node == NULL)
    {
        return NULL;
    }

    return node->data;
}

int destroy_hash_table(Hashtable *hashTable)
{
    if (hashTable == NULL)
    {
        return 0;
    }
    for (int i = 0; i < hashTable->table_size; i++)
    {
        destroy_link_list((hashTable->value_list) + i);
    }
    free(hashTable->value_list);

    return 1;
}

Node *search_node_h(Hashtable *hashTable, void *value)
{
    if (hashTable == NULL)
    {
        return NULL;
    }
    int key = (hashTable->fun)(hashTable, value);
    LinkList *list = hashTable->value_list + key;

    list->p = list->head;
    while (list->p != NULL)
    {
        if (memcmp(list->p->data, value, hashTable->value_size) == 0)
        {
            return list->p;
        }
        list->p = list->p->next;
    }

    return NULL;
}

int hash_int(Hashtable *hashTable, void *value)
{
    return *(int *)(value) % hashTable->table_size;
}

int hash_string(Hashtable *hashTable, void *value)
{
    char *c = value;
    int sum = 0;
    for (int i = 0; *(c + i) != '\0'; i++)
    {
        sum += *(c + i);
    }
    return sum % hashTable->table_size;
}