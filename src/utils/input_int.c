#include "../../include/utils/input_int.h"

char input_num(int *num)
{
    setbuf(stdin, 0);                   //清空输入缓冲区
    char *s = malloc(sizeof(char) * 9); //分配字符串内存
    char c;
    int i = 0;
    int start = 0; //字符串开始标志
    while (1)
    {
        c = getchar();
        if (c == ' ' && !start) //跳过字符串前空格
        {
            continue;
        }
        start = 1;
        if (c >= '0' && c <= '9') //是数字
        {
            *(s + i) = c;
            i++;
        }
        else
        {
            break;
        }
        if (i == 9) //数字达到限制
        {
            break;
        }
    }
    *(s + i) = '\0'; //字符串结束
    *num = atof(s);  //字符串转化为整数
    free(s);         //释放字符串空间
    return c;
}

void input_int(int *num, int length)
{
    for (int i = 0; i < length; i++)
    {
        scanf("%d", num + i);
        getchar();
    }
}