#include "../../include/tools/flushiobuf.h"

void flush_io_buf()
{
    setbuf(stdin, 0);
    setbuf(stdout, 0);
}

void clean_io_buff()
{
    char c;
    while ((c = getchar()) != '\n' && c != '\0')
        ;
}
