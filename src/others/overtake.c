#include "../../include/others/overtake.h"

void overtake_main()
{
    static_arr();
}

void static_arr()
{
    char c;
    int num; //车数量
    printf("Please input car numbers:");
    scanf("%d", &num);

    printf("Please input order of entry:\n"); //驶入的顺序
    int *in_arr = malloc(sizeof(int) * num);
    for (int i = 0; i < num; i++)
    {
        c = input_num(in_arr + i);
    }
    if (c != '\n')
    {
        clean_io_buff();
    }

    printf("Please input order of out:\n"); //驶出的顺序
    int *out_arr = malloc(sizeof(int) * num);
    for (int i = 0; i < num; i++)
    {
        c = input_num(out_arr + i);
    }
    if (c != '\n')
    {
        clean_io_buff();
    }

    int overtake = 0;
    int i = 0, j = 0;
    while (i < num && j < num)
    {
        while (*(in_arr + i) < *(out_arr + j) && j < num)
        {
            j++;
            overtake++;
        }
        while (*(in_arr + i) > *(out_arr + j) && i < num)
        {
            i++;
        }
        i++;
        j++;
    }

    printf("overtake: %d\n", overtake);

    free(in_arr);
    free(out_arr);
}

#define TUNNEL_NUM 10

void dymanic_arr()
{
    CarList *carList = NULL; //进入隧道的链表队列
    Car front = carList;     //链表队列头部
    Car tail = NULL;         //链表队列尾部
    int car_num = 0;         //隧道队列里车的个数

    int type = 0; //0:驶入 1:驶出
    while (1)
    {
        printf("type:");
        scanf("%d", &type);
        if (type == 0)
        {
            if (car_num < TUNNEL_NUM) //可以进入隧道
            {
                Car in_car = malloc(sizeof(Car)); //驶入的车辆
                printf("entry number:");
                int entry_num;
                scanf("%d", &(in_car->number));
                if (car_num == 0)
                {
                    carList = in_car;
                    front = carList; //更新头部
                }
                else
                {
                    tail->next = in_car; //插入尾部
                }
                tail = in_car; //更新尾部
                tail->next = NULL;
                car_num++;
                printf("entry : %d\n", in_car->number);
            }
            else
            {
                printf("wait!\n");
            }
            continue;
        }
        if (type == 1)
        {
            if (car_num == 0)
            {
                printf("no car!\n");
            }
            else
            {
                Car out_car = NULL;    //驶出的车辆
                Car out_before = NULL; //排在驶出车辆前一辆车
                printf("out number:");
                int out_num;
                scanf("%d", &out_num);
                Car p = front;
                while (p != NULL)
                {
                    if (p->number == out_num)
                    {
                        out_car = p;
                        break;
                    }
                    out_before = p;
                    p = p->next;
                }

                if (out_car == NULL)
                {
                    printf("no car!\n");
                }
                else
                {
                    if (out_before == NULL) //未超车
                    {
                        printf("out : %d\n", out_car->number);
                        carList = front->next;
                        front = carList;
                        tail = car_num == 1 ? front : tail; //更新尾部
                    }
                    else //超车
                    {
                        printf("overtake : %d\n", out_car->number);
                        out_before->next = out_car->next;
                        tail = out_before->next == NULL ? out_before : tail; //更新尾部
                    }
                    free(out_car);
                    car_num--;
                }
            }

            continue;
        }
        printf("input error!\n");
    }
}