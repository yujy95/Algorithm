#include "../../include/others/pension.h"

void pension_main()
{
    SEX sex;
    printf("sex:");
    scanf("%d", &sex);
    int age_r = sex == MAN ? AGE_MAN : AGE_WOMEN;

    OP_TYPE type;
    printf("option:");
    scanf("%d", &type);

    if (type == BASE)
    {
        double ave_wages;
        int months;
        double pay;
        double amount;
        int months_l;
        double others;

        printf("ave wages:");
        scanf("%lf", &ave_wages);
        printf("pay months:");
        scanf("%d", &months);
        printf("pay index:");
        scanf("%lf", &pay);
        printf("amonut:");
        scanf("%lf", &amount);
        printf("equal months:");
        scanf("%d", &months_l);
        printf("others:");
        scanf("%lf", &others);

        printf("base pension:%.2lf\n", get_pension(ave_wages, months, pay, amount, age_r, months_l, others));
    }
    if (type == PENSION)
    {
        double base;
        int age;
        printf("base pension:");
        scanf("%lf", &base);
        printf("age:");
        scanf("%d", &age);

        printf("pension:%.2lf\n", up_pension(base, RATE, age - age_r));
    }
    if (type == AGE)
    {
        double base;
        double pension;
        printf("base pension:");
        scanf("%lf", &base);
        printf("pension:");
        scanf("%lf", &pension);

        int year = up_years(base, RATE, pension);
        int age = year > 0 ? year + age_r : year;
        printf("age:%d\n", age);
    }
}

double get_pension(double ave_wages, int months, double pay_index, double amount, int age_r, int months_l, double others)
{
    double base = (ave_wages + ave_wages * pay_index) / 24 * months * 0.01;          //基本性养老金
    double person = amount / (age_r == AGE_MAN ? MONTHS_MAN : MONTHS_WOMAN);         //个人账户养老金
    double transition = (ave_wages + ave_wages * pay_index) / 24 * months_l * 0.014; //过渡性养老金
    return base + person + transition + others;
}

double up_pension(double base, float rate, int year)
{
    for (int i = 0; i < year; i++)
    {
        base *= (1 + rate);
    }
    return base;
}

int up_years(double base, float rate, double pension)
{
    if (pension < base)
    {
        return -1;
    }
    int year = 0;
    for (year = 0; base < pension; year++)
    {
        base *= (1 + rate);
    }
    return year;
}