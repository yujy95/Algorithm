#include "../../include/others/repay.h"

void get_repay(float *a, int n, int m)
{
    int all_num = m;
    int *max_num = malloc(sizeof(int) * n);
    float *max_ave = malloc(sizeof(float) * n);
    float max = 0;
    for (int i = 0; i < n; i++)
    {
        max = 0;
        for (int j = 0; j < all_num; j++)
        {
            if ((*(a + i * all_num + j) / (j + 1)) > max)
            {
                max = *(a + i * all_num + j) / (j + 1);
                *(max_num + i) = j + 1; //资源数量
                *(max_ave + i) = max;   //平均回报
            }
        }
    }

    int p; //位置
    for (int i = 0; i < n; i++)
    {
        max = 0;
        for (int j = 0; j < n; j++)
        {
            if (*(max_ave + j) > max)
            {
                max = *(max_ave + j);
                p = j;
            }
        }
        *(max_ave + p) = -1;    //找到后变为-1
        if (*(max_num + p) > m) //所需资源的个数大于总个数
        {
            *(max_num + p) = m; //分配m个
        }
        m -= *(max_num + p);
    }

    int all = 0;
    for (int i = 0; i < n; i++)
    {
        p = *(max_num + i);
        all += *(a + i * all_num + p - 1);
        printf("给工程%d投入%d份额,", i + 1, p);
    }
    printf("可达到最大利润%d\n", all);
    free(max_num);
    free(max_ave);
}