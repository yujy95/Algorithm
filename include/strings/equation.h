#ifndef _equation_h
#define _equation_h

#include "../container/seqqueue.h"
#include "../container/linkstack.h"
#include <string.h>

void equation_main();
double calculate(char* s);
void postfix_equation(char *s, SeqQueue* seqQueue);
int sign_grade(char c);

#endif