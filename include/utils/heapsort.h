#ifndef _heapsort_h
#define _heapsort_h

#include <stdio.h>

#include "../tools/arrio.h"

void heap_sort(int *a, int n);
void heap_adjust(int *a, int n);

#endif