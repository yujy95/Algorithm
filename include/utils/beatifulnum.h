#ifndef _beautiful_h
#define _beautiful_h

#include <stdio.h>
#include <math.h>

int is_beautiful(int n, int b);

#endif