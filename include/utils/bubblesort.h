#ifndef _bubblesort_h
#define _bubblesort_h

#include <stdio.h>

void bubble_sort(int *a, int n);

#endif