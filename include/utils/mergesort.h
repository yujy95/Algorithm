#ifndef _mergesort_h
#define _mergesort_h

#include <stdlib.h>
#include <stdio.h>

void merge_sort(int *a, int n);
void m_sort(int *a, int low, int high, int *temp);

#endif