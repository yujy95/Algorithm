#ifndef _primenum_h
#define _primenum_h

#include <stdio.h>
#include <math.h>

int is_prime(int num);

#endif