#ifndef _quicksort_h
#define _quicksort_h

#include <stdio.h>

#include "../tools/arrio.h"

void quick_sort(int *a, int n);
void q_sort(int *a, int low, int high);

#endif
