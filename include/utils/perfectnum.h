#ifndef _perfectnum_h
#define _perfectnum_h

#include <stdio.h>
#include <math.h>

int is_perfect_num(int n);

#endif