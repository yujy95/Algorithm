#ifndef _avltree_h
#define _avltree_h

#include <stdio.h>
#include <stdlib.h>

#define MAX(a, b) ((a > b) ? a : b)
#define HIGHT(AVLTree) ((AVLTree == NULL) ? 0 : AVLTree->hight)

typedef struct AVLNode
{
    int position;
    int hight;
    struct AVLNode *lchild; //左孩子
    struct AVLNode *rchild; //右孩子
    struct AVLNode *parent; //父节点
} AVLNode, *AVLTree;

AVLTree add_avl_node(AVLTree avlTree, int position);
int delect_avl_node(AVLTree avlTree, int position);
AVLNode* search_avl_node(AVLTree avlTree, int position);
void printf_avl_tree(AVLTree avlTree);
int destroy_avl_tree(AVLTree avlTree);

AVLTree balance_left(AVLTree avlTree);
AVLTree balance_right(AVLTree avlTree);

#endif