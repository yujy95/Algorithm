#ifndef _hashtable_h
#define _hashtable_h

#include "../container/linklist.h"

typedef enum
{
    INT,
    STRING
} ValueType; //支持的散列类型

typedef struct HashNode
{
    int key;
    void *value;
} HashNode;

typedef struct Hashtable
{
    int table_size;                                       //散列表大小
    int value_size;                                       //value值长度
    LinkList *value_list;                                 //value值链表
    int (*fun)(struct Hashtable *hashTable, void *value); //散列函数
} Hashtable;

int init_hash_table(Hashtable *hashTable, ValueType valueType, int table_size); //初始化散列表
int inseart_hash_node(Hashtable *hashTable, void *value);                       //插入值
int delect_hash_node(Hashtable *hashTable, void *value);                        //删除值
void *search_hash_node(Hashtable *hashTable, void *value);                      //查询值在散列表中存放的地址
int destroy_hash_table(Hashtable *hashTable);                                   //销毁散列表

Node *search_node_h(Hashtable *hashTable, void *value);

int hash_int(Hashtable *hashTable, void *value);    //整型散列函数
int hash_string(Hashtable *hashTable, void *value); //字符串散列函数

#endif