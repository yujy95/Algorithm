#ifndef _shellsort_h
#define _shellsort_h

#include <stdio.h>

void shell_sort(int *a, int n);

#endif