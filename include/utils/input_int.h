#ifndef _input_int_h
#define _input_int_h

#include <stdio.h>
#include <stdlib.h>

char input_num(int *num);
void input_int(int *num, int length);

#endif