#ifndef _palprinum_h
#define _palprinum_h

#include "primenum.h"

int is_pal_pri_num(int n);

#endif