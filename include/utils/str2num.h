#ifndef _str2num_h
#define _str2num_h

#include "../container/linkstack.h"
#include "math.h"

double str_convert(char *s);
int char_int(char c);

#endif