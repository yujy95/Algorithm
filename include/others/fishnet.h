#ifndef _fishnet_h
#define _fishnet_h

#include <stdio.h>
#include "../utils/days.h"

void fish_net(void);
int get_days(int year, int month, int day);
int is_fishing(int days);

#endif