#ifndef _pension_h
#define _pension_h

#include <stdio.h>

#define AGE_MAN 55   //男性退休年龄
#define AGE_WOMEN 50 //女性退休年龄

#define MONTHS_MAN 170   //55岁计发月数
#define MONTHS_WOMAN 195 //50岁计发月数

#define RATE 0.05 //每年上涨系数

typedef enum
{
    MAN,
    WOMAN
} SEX; //性别

typedef enum
{
    BASE,
    PENSION,
    AGE
} OP_TYPE;

void pension_main();

double get_pension(double ave_wages, int months, double pay_index, double amount, int age_r, int months_l, double others); //养老金：平均社评工资、缴纳年限、缴纳指数、个人账户金额、退休年龄、过渡月份、附加项目
double up_pension(double base, float rate, int year);                                                                      //增长固定年限后的养老金
int up_years(double base, float rate, double pension);                                                                     //增长到固定金额养老金需要的年龄

#endif