#ifndef _overtake_h
#define _overtake_h

#include "../utils/input_int.h"
#include "../tools/flushiobuf.h"

#include <stdio.h>
#include <stdlib.h>

typedef struct Car
{
    int number;       //车牌号
    struct Car *next; //下一辆车
} CarList, *Car;

void overtake_main();

void static_arr();
void dymanic_arr();

#endif