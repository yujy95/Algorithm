#ifndef _hanoi_h
#define _hanoi_h

#define move HanoiMove

#include <stdio.h>

void hanoi_main(void);
void hanoi(int i, char a, char b, char c);
void hanoi_move(char x, char b);

#endif