#ifndef _eatpeach_h
#define _eatpeach_h

#include <stdio.h>

void eat_peach(void);
int recursion(int n);
int loop(int n);

#endif