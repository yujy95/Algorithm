#ifndef _more_form_h
#define _more_form_h

#include "../utils/input_int.h"
#include "../tools/flushiobuf.h"

#include <stdio.h>
#include <stdlib.h>

void more_form();

int form_num(int *form, int *type, int length, int sober);

#endif