#ifndef _arrio_h
#define _arrio_h

#include <stdio.h>

void input_arr(int *a, int n);
void output_arr(int *a, int n);

#endif