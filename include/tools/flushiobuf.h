#ifndef _flushiobuf_h
#define _flushiobuf_h

#include <stdio.h>

void flush_io_buf();
void clean_io_buff();

#endif