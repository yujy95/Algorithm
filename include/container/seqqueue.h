#ifndef _seqqueue_h
#define _seqqueue_h

#include "seqlist.h"

typedef struct
{
    SeqList list;
} SeqQueue;

int init_seq_queue(SeqQueue *queue, int datasize);
int push_seq_queue(SeqQueue *queue, void *data);
int pop_seq_queue(SeqQueue *queue);
void *get_seq_front(SeqQueue *queue);
int destroy_seq_queue(SeqQueue *queue);

#endif