/*
 *  Directed graph(Orthogonal table):
 *  seq_node data(vertex*) -> link_node data(edge*)
*/

#ifndef _diregraph_h
#define _diregraph_h

#include "linklist.h"
#include "seqlist.h"

typedef struct Vertex
{
    int num;            //顶点编号
    void *data;         //顶点数据
    LinkList edge_list; //边链表，目的顶点
} Vertex;

typedef struct Edge
{
    int weight;         //权
    Vertex *obj_vertex; //目的顶点
    Vertex *beg_vertex; //起始顶点
} Edge;

typedef struct Diregraph
{
    int datasize;        //数据大小
    SeqList vertex_list; //顶点列表
} Diregraph;

int init_dire_graph(Diregraph *diregraph, int datasize);
int add_dire_vertex(Diregraph *diregraph, int num, void *data);
int add_dire_edge(Diregraph *diregraph, int begin_num, int object_num, int weight);
int delect_dire_vertex(Diregraph *diregraph, int num);
int delect_dire_edge(Diregraph *diregraph, int begin_num, int object_num);
Vertex *search_dire_vertex(Diregraph *diregraph, int num);
int modify_dire_vertex(Diregraph *diregraph, int num, Vertex *new_vertex);
int destroy_dire_graph(Diregraph *diregraph);

#endif