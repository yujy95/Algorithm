#ifndef _linkstack_h
#define _linkstack_h

#include "linklist.h"

typedef struct
{
    LinkList list;
} LinkStack;

int init_link_stack(LinkStack *stack, int datasize);
int push_link_stack(LinkStack *stack, void *);
int pop_link_stack(LinkStack *stack);
void *get_link_top(LinkStack *stack);
int destroy_link_stack(LinkStack *stack);

#endif