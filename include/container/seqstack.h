#ifndef _seqstack_h
#define _seqstack_h

#include "seqlist.h"

typedef struct
{
    SeqList list;
} SeqStack;

int init_seq_stack(SeqStack *stack, int datasize);
int push_seq_stack(SeqStack *stack, void *data);
int pop_seq_stack(SeqStack *stack);
void *get_seq_top(SeqStack *stack);
int destroy_seq_stack(SeqStack *stack);

#endif