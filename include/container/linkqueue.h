#ifndef _linkqueue_h
#define _linkqueue_h

#include "linklist.h"

typedef struct
{
    LinkList list;
} LinkQueue;

int init_link_queue(LinkQueue *queue, int datasize);
int push_link_queue(LinkQueue *queue, void *data);
int pop_link_queue(LinkQueue *queue);
void *get_link_front(LinkQueue *queue);
int destroy_link_queue(LinkQueue *queue);

#endif