#ifndef _linklist_h
#define _linklist_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node
{
    void *data;        //节点数据地址
    struct Node *next; //下一个节点地址
} Node;

typedef struct
{
    int length;   //节点个数
    int datasize; //节点数据大小
    Node *head;   //头节点
    Node *p;      //当前指向节点
    Node *tail;   //尾节点
} LinkList;

int init_link_list(LinkList *list, int datasize);         //初始化
int add_link_head(LinkList *list, void *data);            //头插
int add_link_tail(LinkList *list, void *data);            //尾插
int insert_before(LinkList *list, Node *np, void *data); //前插
int insert_after(LinkList *list, Node *np, void *data);  //后插
int delect_link_node(LinkList *list, Node *np);           //按位置删除
int point_link_node(LinkList *list, Node *np);            //指向指定位置
int point_link_prior(LinkList *list, Node *np);           //指向指定前驱
int point_position(LinkList *list, int position);        //指向制定序号
int destroy_link_list(LinkList *list);                    //销毁链表

Node *creat_link_node(LinkList *list); //创建节点

#endif