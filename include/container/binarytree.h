/*
 *  Binary tree(Adjacency table):
 *  seq_node data(link*) -> link_node data(tree_node*) -> tree_node data(real data*)
*/

#ifndef _binarytree_h
#define _binarytree_h

#include "linklist.h"
#include "seqlist.h"

#include <math.h>

typedef struct BiTNode
{
    void *data;
    int position;           //本层位置,从0开始
    struct BiTNode *lchild; //左孩子
    struct BiTNode *rchild; //右孩子
    struct BiTNode *parent; //父节点
} BiTNode;

typedef struct
{
    int datasize;        //数据大小
    int storey;          //层数，从1开始
    SeqList storey_list; //层节点列表
} BinTree;

int init_bin_tree(BinTree *binTree, int datasize);                           //初始化二叉树
int add_bin_node(BinTree *binTree, int storey, int position, void *data);    //按序插入一个节点
int delect_bin_node(BinTree *binTree, int storey, int position);             //删除一个节点
int delect_last_storey(BinTree *binTree);                                    //删除一最后一行节点
BiTNode *search_bin_node(BinTree *binTree, int storey, int position);        //查询一个节点
int modify_bin_node(BinTree *binTree, int storey, int position, void *data); //修改一个节点
int destroy_bin_tree(BinTree *binTree);                                      //删除二叉树

int add_node_list(BinTree *binTree);
Node *search_node_bin(BinTree *binTree, int storey, int position);
int point_before(LinkList *node_list, int position);

#endif